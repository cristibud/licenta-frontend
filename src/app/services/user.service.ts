import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { UserLogin } from '../models/UserLoginModel';
import { User } from '../models/UserModel';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseURL = 'http://localhost:8080/home/';
  loginUrl = 'login';
  registerUrl = 'register';

  constructor(private http: HttpClient, private router: Router) {
  }

  login(userLogin: UserLogin) {
    const params = new HttpParams().set('email', userLogin.email.toString()).set('password', userLogin.password.toString());
    return this.http.post<UserLogin>(this.baseURL + this.loginUrl, params).pipe(tap(res => this.setSession(res)));
  }

  register(user: User) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json')
    return this.http.post<User>(this.baseURL + this.registerUrl, user, {headers: headers}).pipe(tap(res => this.setSession(res)));
  }

  private setSession(userData: UserLogin) {
    localStorage.setItem('email', userData.email);
  }

  public isLoggedIn() {
    if (!localStorage['email']) {
      return false;
    }
    return true;
  }

  public logout() {
    localStorage.removeItem('email');
    window.location.replace("http://localhost:4200");
  }

}
