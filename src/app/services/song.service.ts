import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Song } from '../models/SongModel';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  baseURL = 'http://localhost:8080/song';
  youtubeURL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics&id=';
  youtubeRecommandation = 'https://www.googleapis.com/youtube/v3/playlists?part=snippet,contentDetails&channelId='
  apiKey = '&key=AIzaSyA8Ql7xrMnWDJ4K7pou-yleCcc08H1ZfNQ'
  constructor(private http: HttpClient) {
  }
  
  refresh(): void {
      window.location.reload();
  }
  allSongs() {
    let email = localStorage.getItem('email');
    const params = new HttpParams().set('email', email);
    return this.http.get <Song[]>(this.baseURL + '/allSongs', {params});
  }

  updateSong(song: Song) {
    return this.http.put<Song>(this.baseURL + '/updateSong', song, {
      headers: new HttpHeaders(
        {'Content-Type': 'application/json'}
      )
    }).subscribe(data => this.refresh());
  }

  addSong(song: Song) {
    let email = localStorage.getItem('email');
    return this.http.post<Song>(this.baseURL + '/addSong', song, {
      headers: new HttpHeaders(
        {'Content-Type': 'application/json'}
      ),
      params: new HttpParams().set('email', email)
    })
      .subscribe(data => this.refresh());
  }

  deleteSong(id: number) {
    const params = new HttpParams().set('id', id.toString());
    console.log('am ajuns aici')
    return this.http.delete(this.baseURL + '/delete', {params}).subscribe(data => this.refresh());
  }

  getYoutubeSong(id: string) {
    let song: Song = new Song
    return new Promise<Song>(resolve => {
      this.http.get(this.youtubeURL + id + this.apiKey)
        .subscribe(data =>
          { 
            song.title = data['items'][0]['snippet']['title'];
            song.channelId = data['items'][0]['snippet']['channelId'];
            song.views = data['items'][0]['statistics']['viewCount']
            song.likes = data['items'][0]['statistics']['likeCount']
            song.dislikes = data['items'][0]['statistics']['dislikeCount']
            song.songId = id;
            song.genre = 'Electronic'
            resolve(song)
          }
        )
    })
  }

  viewRecommandation(channelId: string) {
    return new Promise<Song>(resolve => {
      this.http.get(this.youtubeRecommandation + channelId + this.apiKey)
        .subscribe(data =>
          { 
            resolve(data['items'][0].id);
          }
        )
    })
  }
}
