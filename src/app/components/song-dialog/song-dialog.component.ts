import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-song-dialog',
  templateUrl: './song-dialog.component.html',
  styleUrls: ['./song-dialog.component.css']
})
export class SongDialogComponent {
  URL: SafeResourceUrl;

  constructor(
    public dialogRef: MatDialogRef<SongDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: String,
    private sanitizer: DomSanitizer) {
      this.URL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + data.toString() + '?autoplay=1&enablejsapi=1');
    }

  onNoClick(): void {
      this.dialogRef.close();
  }
}