import { Component, Inject } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.css']
})
export class RecommendationComponent {
  URL: SafeResourceUrl;

  constructor(
    public dialogRef: MatDialogRef<RecommendationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: String,
    private sanitizer: DomSanitizer) {
      this.URL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/videoseries?list=' + data.toString() );
    }

  onNoClick(): void {
      this.dialogRef.close();
  }
}
