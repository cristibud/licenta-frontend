import { Component, OnInit } from '@angular/core';
import { SongService } from 'src/app/services/song.service';
import { UserService } from 'src/app/services/user.service';
import { Song } from 'src/app/models/SongModel';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})

export class StatisticsComponent implements OnInit {
  public chartType: string = 'bar';
  favoriteGenre: string;
  datas: Song[];
  populatedData: boolean = false;
  genrePopularityOrdered = [0, 0, 0, 0, 0, 0, 0, 0];
  genreList:  Array<{id: number, text: string}> = [
    {id: 0, text: 'Blues'},
    {id: 1, text: 'Country'},
    {id: 2, text: 'Electronic'},
    {id: 3, text: 'Hip Hop'},
    {id: 4, text: 'Jazz'},
    {id: 5, text: 'Metal'},
    {id: 6, text: 'Pop'},
    {id: 7, text: 'Rock'},
];
  public chartDatasets: Array<any> = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0], label: '' }
  ];

  public chartLabels: Array<any> = ['Blues', 'Country', 'Electronic', 'Hip Hop', 'Jazz', 'Metal', 'Pop', 'Rock'];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(153, 12, 64, 0.2)',
        'rgba(155, 79, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
        'rgba(153, 12, 64, 1)',
        'rgba(155, 79, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  constructor(private userService: UserService, private SongService: SongService) { }

  ngOnInit() {
    this.allSongs().subscribe(data =>
      { 
        this.datas = data;
        this.datas.forEach(element => {
          this.genreList.forEach(el => {
            if(el.text == element.genre) {
              this.chartDatasets[0].data[el.id]++;
              this.genrePopularityOrdered[el.id]++;
            }
          });
        });
        this.genrePopularityOrdered.sort((a,b) => {
          if (a > b) {
            return 1;
          }
          if (a < b) {
              return -1;
          }
            return 0;
        });
        var index = 0, favouriteNo;
        this.chartDatasets[0].data.forEach(element => {
          if(element == this.genrePopularityOrdered[7]) {
            this.favoriteGenre = this.chartLabels[index] 
          }
          index++;
        });
        this.populatedData = true;
      }
    )
  }

  allSongs() {
    return this.SongService.allSongs();
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }
}
