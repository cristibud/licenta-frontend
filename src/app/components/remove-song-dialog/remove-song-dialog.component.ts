import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SongService } from 'src/app/services/song.service';

@Component({
  selector: 'app-remove-song-dialog',
  templateUrl: './remove-song-dialog.component.html',
  styleUrls: ['./remove-song-dialog.component.css']
})
export class RemoveSongDialogComponent {
  constructor(
    public SongSevice: SongService,
    public dialogRef: MatDialogRef<RemoveSongDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number,
  ) {

  }

  onNoClick(): void {
      this.dialogRef.close();
  }

  deleteSong(): void {
    this.SongSevice.deleteSong(this.data);
  }

}
