import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { SongService } from '../../services/song.service';
import { MatPaginator } from '@angular/material/paginator';
import { SongDialogComponent } from '../song-dialog/song-dialog.component';
import { RecommendationComponent } from '../recommendation/recommendation.component';
import { Song } from 'src/app/models/SongModel';
import { RemoveSongDialogComponent } from '../remove-song-dialog/remove-song-dialog.component';


export interface SongList {
  title: string;
  id: number;
  genre: string;
  views: number;
  likes: number;
  dislikes: number;
  songID: String;
}


@Component({
  selector: 'app-Song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css']
})
export class SongsComponent implements OnInit {
  Song: Song;
  error: boolean;
  errorMessage: string;
  displayedColumns: string[] = ['title', 'genre', 'views', 'likes', 'dislikes', 'play', 'remove', 'recommandation'];
  datas: Song[];
  dataSource: any;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService, public SongSevice: SongService, public dialog: MatDialog) {
    this.allSongs().subscribe(data =>
      {this.datas = data;
      this.dataSource = new MatTableDataSource<Song>(this.datas);
      this.dataSource.paginator = this.paginator;}
    )
  }

  ngOnInit() {
    this.allSongs();
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  logout() {
    this.userService.logout();
  }

  allSongs() {
    return this.SongSevice.allSongs();
  }

  addSong(youtubeURL: string){
    this.SongSevice.getYoutubeSong(this.getYoutubeID(youtubeURL)).then(
      data => this.SongSevice.addSong(data)
    )
  }

  getYoutubeID(youtubeURL: string){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = youtubeURL.match(regExp);
    return (match&&match[7].length==11)? match[7] : 'undefined';
  }

  playSong(id): void {
    const dialogRef = this.dialog.open(SongDialogComponent, {
      width: '1280px',
      data: id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  removeSong(id: number): void {
    const dialogRef = this.dialog.open(RemoveSongDialogComponent, {
      width: '300px',
      height: '200px',
      data: id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  playRecommandation(id): void {
    const dialogRef = this.dialog.open(RecommendationComponent, {
      width: '1280px',
      data: id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  viewRecommandation(id) {
    this.SongSevice.viewRecommandation(id).then(
      data => this.playRecommandation(data)
    );
  }

}
