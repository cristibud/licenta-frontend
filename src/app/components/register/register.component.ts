import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/UserModel';
import { UserLogin } from 'src/app/models/UserLoginModel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userLogin: UserLogin;
  user: User;

  error: boolean;
  errorMessage: string;

  constructor(private userService: UserService,
              private router: Router) {

    this.user = {
      name: '',
      email: '',
      password: '',
      confirmationPassword: ''
    };

    this.error = false;
    this.errorMessage = '';
  }

  displayError() {
    return this.error;
  }

  displayErrorMessage() {
    return this.errorMessage;
  }

  validateEmail() {
    const regex = new RegExp('^[A-Za-z0-9._%+-]+@gmail.com$');
    return regex.test(this.user.email);
  }

  validatePassword() {
    if (this.user.password.length >  8) {
      return true;
    } else {
      return false;
    }
  }

  validateConfirmationPassword() {
    if (this.user.password === this.user.confirmationPassword) {
      return true;
    } else {
      return false;
    }
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  submitForm() {
    if(!this.validateConfirmationPassword() || !this.validateEmail() || !this.validatePassword()) {
      return;
    }
    console.log(this.user)
    this.userService.register(this.user).subscribe(
      data => {
        this.error = false;
        this.router.navigate(['playlist']);
      },
      err => {
        this.error = true;
        if (err.valueOf().error === 'EMAIL_NOT_VALID') {
          this.errorMessage = 'Email is incorrect.';
        } else if (err.valueOf().error === 'PASSWORD_NOT_VALID') {
          this.errorMessage = 'Password is incorrect.';
        } else {
          this.errorMessage = "Something doesn't work!";
        }

      }
    );
  }

  ngOnInit() {

  }
}
