import {Component} from '@angular/core';
import {LoginComponent} from './components/login/login.component';
import {MatDialog} from '@angular/material';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FindGenre';
  description = 'FindGenre';

  constructor(private loginService: UserService,public dialog: MatDialog) {

  }

  isLoggedIn() {
    return this.loginService.isLoggedIn();
  }
  openDialog(): void {
    const dialog = this.dialog.open(LoginComponent, {
      width: '400px'
    });
  }
  logout() {
    this.loginService.logout();
  }


}
