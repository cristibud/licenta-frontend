import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { SongsComponent } from './components/song/song.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { SongDialogComponent } from './components/song-dialog/song-dialog.component';
import { RegisterComponent } from './components/register/register.component';




const routes: Routes = [
  {path: '', redirectTo: 'custosmer', pathMatch: 'full'},
  {path: '', component: LoginComponent},
  {path: 'playlist', component: SongsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: 'dialog', component: SongDialogComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
