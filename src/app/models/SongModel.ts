export class Song {
    title: string;
    channelId: string;
    id: number;
    genre: string;
    views: number;
    likes: number;
    dislikes: number;
    songId: String;
  }