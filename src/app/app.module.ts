import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './components/login/login.component';
import {MatCardModule} from '@angular/material/card';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {SongsComponent} from './components/song/song.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { RecommendationComponent } from './components/recommendation/recommendation.component';
import { SongDialogComponent } from './components/song-dialog/song-dialog.component';
import { RegisterComponent } from './components/register/register.component';
import { RemoveSongDialogComponent } from './components/remove-song-dialog/remove-song-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SongsComponent,
    StatisticsComponent,
    RecommendationComponent,
    SongDialogComponent,
    RegisterComponent,
    RemoveSongDialogComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MatDialogModule,
    MatCardModule,
    MatDialogModule,
    MatCardModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatButtonModule,
    MatCheckboxModule,
    MatMenuModule,
    MatToolbarModule,
    HttpClientModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule
  ],
  entryComponents: [
    RecommendationComponent, 
    RemoveSongDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    RouterModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatCheckboxModule,
    MatMenuModule,
    MatToolbarModule,
  ]
})
export class AppModule {
}
